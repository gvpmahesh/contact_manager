require 'csv'

class ContactsCsvImporter
  MAX_RECORDS = 100
  MAX_FILE_SIZE = 1.megabytes
  PRETTY_MAX_FILE_SIZE = "1 MB".freeze

  attr_reader :file, :user

  def initialize(file, user)
    @file = file
    @user = user
  end

  def process
    issues_with_input = false
    output_file = File.open("tmp/output_#{DateTime.now.to_i}.csv", "w")
    csv = CSV.open(output_file, 'w') do |csv|
      csv << Contact::CSV_FILE_HEADERS
      CSV.foreach(file.path, headers: true) do |row|
        next if row["record_id"]

        contact = Contact.create_csv_record!(row, user.id)
        csv << contact.as_csv_row + success_metadata
      rescue => e
        issues_with_input = true
        csv << [record_id = nil, row['email'], row['first_name'], row['last_name'], row['phone_number'], 'failure'.freeze, e.message]
      end
    end

    csv_upload_report = user.csv_upload_reports.create(bad_data: issues_with_input)
    csv_upload_report.report.attach(io: File.open(output_file), filename: "output_#{DateTime.now.to_i}.csv")

    File.delete(output_file)

    csv_upload_report
  end

  def exceeds_max_records?
    CSV.read(file).length > 100
  end

  def exceeds_max_file_size?
    file.size > MAX_FILE_SIZE
  end

  def valid_file_extension?
    File.extname(file) == ".csv"
  end

  def valid_headers?
    CSV.open(file, &:readline).all? { |header| header.in? Contact::CSV_FILE_HEADERS }
  end

  private
    def success_metadata
      @success_metadata ||= %w[success none]
    end
end
