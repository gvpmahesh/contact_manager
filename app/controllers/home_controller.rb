class HomeController < ApplicationController
  before_action :check_logged_in_user

  def show
  end

  private
    def check_logged_in_user
      redirect_to contacts_path if current_user
    end
end
