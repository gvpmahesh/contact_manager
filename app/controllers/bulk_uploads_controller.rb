class BulkUploadsController < ApplicationController
  before_action :authenticate_user!
  before_action :validate_file, only: [:create]
  before_action :validate_csv_headers, only: [:create]

  def show
    @csv_upload_reports = current_user.latest_upload_reports
  end

  def create
    report = csv_importer.process
    if report.bad_data?
      redirect_to bulk_uploads_path, alert: 'Some of the records are not processed. Please see the report below for details, and re-upload the report post fixing the errors'
    else
      redirect_to contacts_path, notice: 'Successfully uploaded all the contacts, Please see the report in bulk uploads page for further details'
    end
  end

  private
    def csv_importer
      @csv_importer ||= ContactsCsvImporter.new(file, current_user)
    end

    def file
      params[:file]
    end

    def validate_csv_headers
      unless csv_importer.valid_headers?
        redirect_to bulk_uploads_path, notice: 'Please re-upload the csv file with valid headers'
        return
      end
    end

    def validate_file
      unless csv_importer.valid_file_extension?
        redirect_to bulk_uploads_path, notice: 'Only CSV files are allowed'
        return
      end

      if csv_importer.exceeds_max_file_size?
        redirect_to bulk_uploads_path, notice: "Max file size is #{ContactsCsvImporter::PRETTY_MAX_FILE_SIZE}"
        return
      end

      if csv_importer.exceeds_max_records?
        redirect_to bulk_uploads_path, notice: "Cannot process more than #{ContactsCsvImporter::MAX_RECORDS} records at once"
        return
      end
    end
end
