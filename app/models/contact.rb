class Contact < ApplicationRecord
  CSV_FILE_HEADERS = %w[record_id email first_name last_name phone_number status error].freeze

  belongs_to :user

  validates :first_name, presence: true, uniqueness: {scope: [:last_name, :user_id] }
  validates :last_name, presence: true
  validates :phone_number, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, uniqueness: {scope: :user_id}

  def self.create_csv_record!(csv_row, user_id)
    create! csv_row.to_hash.except(*meta_csv_fields).merge(user_id: user_id)
  end

  def self.meta_csv_fields
    %w[record_id status error]
  end

  def as_csv_row
    [id, email, first_name, last_name, phone_number]
  end

  private_class_method :meta_csv_fields
end
