class CsvUploadReport < ApplicationRecord
  belongs_to :user
  has_one_attached :report

  def self.latest_upload_reports
    order(created_at: :desc).limit(5)
  end
end
