Rails.application.routes.draw do
  resources :contacts
  resource :bulk_uploads, only: [:show, :create]
  devise_for :users
  root to: "home#show"
end
