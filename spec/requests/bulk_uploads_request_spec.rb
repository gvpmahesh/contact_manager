require 'rails_helper'

RSpec.describe "BulkUploads", type: :request do
  before do
    @user = create(:user)
    sign_in @user
  end

  describe "GET /show" do
    it "returns http success" do
      get "/bulk_uploads"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST create" do
    let(:valid_csv) { fixture_file_upload(file_fixture('valid.csv')) }
    let(:invalid_csv) { fixture_file_upload(file_fixture('invalid.csv')) }

    it "redirects with success message for a valid csv file" do
      post bulk_uploads_path, params: { file: valid_csv }
      
      expect(response).to redirect_to contacts_path
      expect(flash[:notice]).to eq "Successfully uploaded all the contacts, Please see the report in bulk uploads page for further details"
    end

    it "redirects with failure message for an invalid csv file" do
      post bulk_uploads_path, params: { file: invalid_csv }
      
      expect(response).to redirect_to bulk_uploads_path
      expect(flash[:alert]).to eq "Some of the records are not processed. Please see the report below for details, and re-upload the report post fixing the errors"
    end
  end
end
