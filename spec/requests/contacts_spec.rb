 require 'rails_helper'

RSpec.describe "/contacts", type: :request do
  before do
    @user = create(:user)
    sign_in @user
  end

  it "user must be authenticated to visit perform any action on contacts" do
    sign_out @user

    get contacts_url

    expect(response).to redirect_to(new_user_session_path)
  end

  describe "GET /index" do
    it "renders a successful response" do
      get contacts_url

      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      contact = create(:contact, user: @user)

      get contact_url(contact)

      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_contact_url

      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "render a successful response" do
      contact = create(:contact, user: @user)

      get edit_contact_url(contact)

      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Contact" do
        expect {
          post contacts_url, params: { contact: attributes_for(:contact, user: @user) }
        }.to change(Contact, :count).by(1)
      end

      it "redirects to the created contact" do
        post contacts_url, params: { contact: attributes_for(:contact, user: @user) }

        expect(response).to redirect_to(contact_url(Contact.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Contact" do
        expect {
          post contacts_url, params: { contact: attributes_for(:contact, first_name: nil) }
        }.to change(Contact, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post contacts_url, params: { contact: attributes_for(:contact, email: nil) }

        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        {
          first_name: 'updated john'
        }
      }

      it "updates the requested contact" do
        contact = create(:contact, user: @user)

        patch contact_url(contact), params: { contact: new_attributes }

        contact.reload
        expect(contact.first_name).to eq 'updated john'
      end

      it "redirects to the contact" do
        contact = create(:contact, user: @user)

        patch contact_url(contact), params: { contact: new_attributes }

        contact.reload
        expect(response).to redirect_to(contact_url(contact))
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        contact = create(:contact, user: @user)
        patch contact_url(contact), params: { contact: {first_name: nil} }

        expect(response).to be_successful
        expect(contact.first_name).not_to eq nil
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested contact" do
      contact = create(:contact, user: @user)

      expect {
        delete contact_url(contact)
      }.to change(Contact, :count).by(-1)
    end

    it "redirects to the contacts list" do
      contact = create(:contact, user: @user)

      delete contact_url(contact)
      expect(response).to redirect_to(contacts_url)
    end
  end
end
