require 'rails_helper'

RSpec.describe ContactsCsvImporter, type: :service do
  let(:file_path) { File.open("tmp/output.csv", "w") }
  let!(:csv) do
    CSV.open(file_path, "w") do |csv|
      csv << ["email", "first_name", "last_name", "phone_number"]
      csv << ["hello@example.com","john","snow","123"]
    end
  end

  let(:bad_file) { File.open("tmp/bad.csv", "w")}
  let!(:bad_csv) do
    CSV.open(bad_file, "w") do |csv|
      csv << ["email", "first_name", "last_name", "phone_number"]
      csv << ["","john","snow","123"]
    end
  end

  let(:user) { create(:user) }

  describe "#process" do
    it "returns the csv upload report with bad_data as false" do
      upload_report = ContactsCsvImporter.new(file_path, user).process
      
      expect(upload_report.bad_data).to eq false

      csv_row =  CSV.parse(upload_report.report.download, headers: true).first

      expect(csv_row["record_id"]).not_to be_nil
      expect(csv_row["status"]).to eq "success"
      expect(csv_row["error"]).to eq "none"
      expect(csv_row["email"]).to eq "hello@example.com"
      expect(csv_row["first_name"]).to eq "john"
      expect(csv_row["last_name"]).to eq "snow"
      expect(csv_row["phone_number"]).to eq "123"

      File.delete(file_path)
    end

    it "returns the csv upload report with bad_data as true" do
      upload_report = ContactsCsvImporter.new(bad_file, user).process
      
      expect(upload_report.bad_data).to eq true

      csv_row =  CSV.parse(upload_report.report.download, headers: true).first

      expect(csv_row["record_id"]).to be_nil
      expect(csv_row["status"]).to eq "failure"
      expect(csv_row["error"]).to eq "Validation failed: Email is invalid"

      File.delete(bad_file)
    end
  end

  describe 'config' do
    it { expect(ContactsCsvImporter::MAX_RECORDS).to eq 100 }
    it { expect(ContactsCsvImporter::MAX_FILE_SIZE).to eq 1.megabytes }
    it { expect(ContactsCsvImporter::PRETTY_MAX_FILE_SIZE).to eq "1 MB" }
  end

  describe "#exceeds_max_records?" do
    it do
      expect(ContactsCsvImporter.new(file_path, user).exceeds_max_records?).to eq false

      CSV.stub_chain(:read, :length).and_return(101)

      expect(ContactsCsvImporter.new(file_path, user).exceeds_max_records?).to eq true
    end
  end

  describe "#exceeds_max_file_size?" do
    it do
      expect(ContactsCsvImporter.new(file_path, user).exceeds_max_file_size?).to eq false

      allow(file_path).to receive(:size).and_return(2.megabytes)

      expect(ContactsCsvImporter.new(file_path, user).exceeds_max_file_size?).to eq true
    end
  end

  describe "#valid_file_extension?" do
    it do
      expect(ContactsCsvImporter.new(file_path, user).valid_file_extension?).to eq true

      allow(File).to receive(:extname).and_return("foo")

      expect(ContactsCsvImporter.new(file_path, user).valid_file_extension?).to eq false
    end
  end

  describe "#valid_headers?" do
    let!(:bad_headers_file) { File.open("tmp/bad_headers.csv", "w")}
    let!(:bad_headers_csv) do
      CSV.open(bad_headers_file, "w") do |csv|
        csv << ["email", "first_name", "last_name", "phone_number", "foo"]
      end
    end

    it do
      expect(ContactsCsvImporter.new(file_path, user).valid_headers?).to eq true

      expect(ContactsCsvImporter.new(bad_headers_file, user).valid_headers?).to eq false

      File.delete(bad_headers_file)
    end
  end
end
