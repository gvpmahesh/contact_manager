require 'rails_helper'

RSpec.describe User, type: :model do
  it 'accepts only valid email' do
    expect(build(:user, email: 'hello@example.com')).to be_valid

    user_with_invalid_email = build(:user, email: 'foo')

    expect(user_with_invalid_email).to be_invalid
    expect(user_with_invalid_email.errors.full_messages).to eq ["Email is invalid"]
  end
end
