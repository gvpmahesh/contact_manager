require 'rails_helper'
require 'csv'

RSpec.describe Contact, type: :model do
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }

  describe "validations" do
    it 'should have a unique full name scoped to a user' do
      create(:contact, first_name: 'John', last_name: 'Mountain', user: user1)
      
      different_user_contact_with_same_name = build(:contact, first_name: 'John', last_name: 'Mountain', user: user2)

      expect(different_user_contact_with_same_name).to be_valid

      duplicate_contact_name = build(:contact, first_name: 'John', last_name: 'Mountain', user: user1)

      expect(duplicate_contact_name).to be_invalid
      expect(duplicate_contact_name.errors.full_messages).to eq ["First name has already been taken"]
    end

    it 'should have unique email scoped to user' do
      create(:contact, email: 'foo@example.com', user: user1)

      user1_duplicate_email_contact = build(:contact, email: 'foo@example.com', user: user1)

      expect(user1_duplicate_email_contact).to be_invalid
      expect(user1_duplicate_email_contact.errors.full_messages).to eq ["Email has already been taken"]

      expect(build(:contact, email: 'foo@example.com', user: user2)).to be_valid
    end
  end

  describe "#as_csv_row" do
    it "returns data for csv row" do
      contact = create(
        :contact,
        first_name: 'john',
        last_name: 'snow',
        email: 'hello@example.com',
        phone_number: 123
      )

      expect(contact.as_csv_row).to eq [contact.id, 'hello@example.com', 'john', 'snow', '123']
    end
  end

  describe 'csv file headers' do
    it do
      expect(Contact::CSV_FILE_HEADERS).to eq %w[record_id email first_name last_name phone_number status error]
    end
  end

  describe '.create_csv_record!' do
    it 'creates and returns a contact record for a valid csv row' do
      csv_row = CSV::Row.new(Contact::CSV_FILE_HEADERS, [nil, 'foo@example.com', 'john', 'snow', '123'])

      contact = Contact.create_csv_record!(csv_row, user1.id)

      expect(contact.user_id).to eq user1.id
      expect(contact.email).to eq 'foo@example.com'
      expect(contact.first_name).to eq 'john'
      expect(contact.last_name).to eq 'snow'
    end

    it 'raises exception for an invalid csv row' do
      csv_row = CSV::Row.new(Contact::CSV_FILE_HEADERS, [nil, 'invalid email', 'john', 'snow', '123'])

      expect { Contact.create_csv_record!(csv_row, user1.id) }.to raise_error
    end
  end
end
