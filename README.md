I have scoped down the probem to limit the quantity of records in bulk uploads, to process the file inilne.

The output file will be downloaded to the browser post processing of bulk uploads. It has `status` and `error` message.

If applicable, the ouput file can be corrected manually and uploaded back.

`record_id` is also present in output file for verification purposes.

Demo: https://simple-contact-manager.herokuapp.com/

Sample csv for bulk upload: https://drive.google.com/file/d/1l0Rc1-FS8eFOYU5WcPNteb221YcX5ZVr/view?usp=sharing

How this can be extended:

* Multiple emails/phones for a contact
* Pagination
* Authorization for contacts controller
* A background job to process the csv file and communicate the result via email or socket
* Real time update of current progress
* End to end tests
