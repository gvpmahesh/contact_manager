class AddUniqueIndexesToContacts < ActiveRecord::Migration[6.0]
  def change
    add_index :contacts, [:first_name, :last_name, :user_id], unique: true
    add_index :contacts, [:email, :user_id], unique: true
  end
end
