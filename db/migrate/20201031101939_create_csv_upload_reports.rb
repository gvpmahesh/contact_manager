class CreateCsvUploadReports < ActiveRecord::Migration[6.0]
  def change
    create_table :csv_upload_reports, id: :uuid do |t|
      t.references :user, null: false, type: :uuid, foreign_key: true
      t.boolean :bad_data, null: false, default: false

      t.timestamps
    end
  end
end
